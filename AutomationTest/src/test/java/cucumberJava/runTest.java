package cucumberJava;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber; 

@RunWith(Cucumber.class) 
@Cucumber.Options(
        features="src/test/java/Features",
        glue=("StepDefinitions"),
        monochrome=true,
        plugin={"pretty",
                "html:target/reports"},
        tags="@demo"
        format = {"pretty", "html:target/cucumber"}
        ) 

public class runTest { }