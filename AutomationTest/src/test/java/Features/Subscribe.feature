Feature: feature to perform the Subscribe operation
@demo
Scenario: Validate the Subscribe functionality 

Given I navigate to the PwC Digital Pulse website
And I am viewing the ‘Home’ page

When I click on the ‘Subscribe’ navigation link

Then I am taken to the ‘Subscribe’ page
And I am presented with the below eight fields
	Field 			Required 	Type
	First name 		true 		text
	Last name 		true 		text
	Organisation 	true 		text
	Job title 		true 		text
	Business email 
	address 		true 		text
	State 			true 		dropdown
	Country 		true 		dropdown
And I will need to complete Google reCAPTCHA before I can complete my request