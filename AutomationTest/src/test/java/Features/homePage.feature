Feature: Feature to view the PwC Digital Pulse home page with 3 columns of articles
@demo
Scenario: Check home page exists with 3 columns of articles

Given I navigate to the PwC Digital Pulse website

When I am viewing the Home page

Then I am presented with 3 columns of articles
And The ‘left’ column is displaying 2 articles
And the ‘middle’ column is displaying 1 article
And The ‘right’ column is displaying 4 articles